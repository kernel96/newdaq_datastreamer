# README 
## Introduction 

The new temperature data acquistion module has been configured to work and save data with a raspberry pi. 

To fascilitate easy access to data and live plotting, two scripts were written. One is the 'server script' to run on the raspberry pi and the other is the 'reciver script' which runs on the TVAC computer. 

To clone this repo with git(use the git bash shell on windows):
```
git clone https://bitbucket.org/kernel96/newdaq_datastreamer.git
cd newdaq_datastreamer
```
---

### Server 
The server script is located in '/home/pi/newdaq_datastreamer/server_socket.py'. It take arguments for the file to stream with '-i'.

The server only transmits the last line of the file when its updated, so it needs to run continuously parallel to the './tempsens16.c' script

To start the server:
```
python server_socket.py -i /home/pi/tempsens/xxx.csv
Stop with: Ctrl + C
```
---

### Client

The client side script is located at 'D:\ProgramFiles\newdaq_datastreamer\receiver_socket.py'.  It takes arguments for HOST IP(-hid), PORT(-p) and name of the output file(-o). The hostIP and port are already pre-configured and should be used only if something changes.

To start listening to server:
```
python receiver_socket.py -o xxxxx.csv
Stop with: Ctrl + C

* Do not open this file with MS excel while the script is running, the script will crash.
```
---

### Visualization

The data is saved with timestamps in the first column and a header is added when saving to the TVAC computer with the reciever script. This allows the use of MCC-DLOGGER visualization application(pyvisualizer.py).

Another script has been written to plot the data(temp_plot.py), which converts to proper data/time and refreshes to plot live data as well. It takes the argument for data to plot with '-i'. 

To plot the data:
```
python temp_plot.py -i pi_data_xxx.csv
```
