# Script for plotting live temperatures with correct date/time format 
#
# Ayush Jain 2020
# Tartu Observatory, Laboratory of Space Technology
# 

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.dates import date2num, DateFormatter
import datetime
from itertools import islice 
import argparse
import time
import csv


parser = argparse.ArgumentParser()

parser.add_argument('-i', '--infile', type=str)
parser.add_argument('-s', '--skip', type=int)
parser.add_argument('-u', '--update', type=int)

args = parser.parse_args()

if not args.infile:
    inputfile = r'C:\Users\labuser\Desktop\pi_data_stream.csv'
else: 
    inputfile = args.infile

if not args.skip: # data skip for faster plotting
    data_skip = 1
else:
    data_skip = args.skip

if not args.update: # update time for graph in seconds   
    update_time = 10
else:
    update_time = args.update
    
        
fig = plt.figure()
ax1 = fig.add_subplot(111)

def animate(i):

    x = []
    y = [] 
    a = []
    b = []
    c = []
    d = []
    e = []
    f = []
    cjc = []

    with open(inputfile, 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter = ',', quotechar ='"')
        for row in islice(plots, 1 , None):
            if len(row) > 1:    
                x.append(float(row[0]))
                y.append(float(row[6]))
                a.append(float(row[15]))
                b.append(float(row[24]))
                c.append(float(row[27]))
                d.append(float(row[30]))
                e.append(float(row[39]))
                f.append(float(row[48]))
                cjc.append(float(row[2]))
    conv_time = []
    for n in x:
        value = datetime.datetime.fromtimestamp(n)
        conv_time.append(date2num(value))
    
    x = conv_time  
    x = x[::data_skip]
    y = y[::data_skip]
    a = a[::data_skip]
    b = b[::data_skip]
    c = c[::data_skip]
    d = d[::data_skip]
    e = e[::data_skip]
    f = f[::data_skip]    
    cjc = cjc[::data_skip]
    ax1.clear()
    ax1.plot(x, y, label='ch00/ v00003')
    ax1.plot(x, a, label='ch03/ v00002 top')
    ax1.plot(x, b, label='ch06/ v51440 top')    
    ax1.plot(x, c, label='ch07/ v00003 top')    
    ax1.plot(x, d, label='ch08/ v51440')
    ax1.plot(x, e, label='ch11/ base plate')
    ax1.plot(x, f, label='ch14/ v00002')
    # ax1.plot(x, b,'g', label='ch03')
    ax1.plot(x, cjc, linestyle='-.', label='cjc', linewidth=0.9)

    ticksSkip = len(conv_time)//15
    ticksUsed = conv_time[::ticksSkip]
    tickLabel = [ i for i in ticksUsed]
    ax1.set_xticks(ticksUsed)
    ax1.set_xticklabels(tickLabel)
    fig.autofmt_xdate()
    ax1.xaxis.set_major_formatter(DateFormatter('%m/%d %H:%M'))
    
    ax1.grid()
    ax1.legend()
    ax1.set_xlabel('time (s)')
    ax1.set_ylabel('temperature (*C)')


ani = animation.FuncAnimation(fig, animate, interval = update_time*1000)
plt.show()
