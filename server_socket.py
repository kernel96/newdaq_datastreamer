# Server soceket script for sending last line of the data from the newDaq 
#
# Ayush Jain 2020
# Tartu Observatory, Laboratory of Space Technology
# 

import socket
import csv
from thread import *
import time 
import argparse


def clientthread(conn):
# sending to connected client
    
    last_line = ''
    while True:
        lis = list(csv.reader(open(inputfile)))
        new_last_line = lis[-1] # copying the last line in the file
        data = new_last_line 

        if new_last_line != last_line: # check if the data has changed
            data = str(data)
            # print len(data)

            conn.send(data)
            last_line = new_last_line
            time.sleep(2)

        else:
            print ('no new data')
            time.sleep(2)
    
    conn.close()


parser = argparse.ArgumentParser()

parser.add_argument('-i', '--infile', type=str)
args = parser.parse_args()

# Take the argument for the input file location 
if not args.infile:
    inputfile = '/home/pi/tempsens/newoiltest_20200911.csv'
else: 
    inputfile = args.infile


HOST = ''                 # Symbolic name meaning the local host
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# avoid socket blocking
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.bind((HOST, PORT))

s.listen(1)
print ('socket now listening')
print('press Ctrl + C to exit')

try:
    while 1:
        conn, addr = s.accept()
        print ('Connected with ' + addr[0] + ':' + str(addr[1]))

        # start new thread for every client connect 
        start_new_thread(clientthread, (conn,))

    s.close()

except KeyboardInterrupt or socket.error:
    s.close()
    print ('closing server')
    
