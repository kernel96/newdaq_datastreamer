# Receiver socket script for sending last line of the data from the newDaq 
#
# Ayush Jain 2020
# Tartu Observatory, Laboratory of Space Technology
# 

import socket
import csv
import numpy as np
import os
import argparse


def file_empty_check(filename_to_check):
    # For checking if the file is empty or not
    status = os.stat(filename_to_check).st_size == 0 
    return status


parser = argparse.ArgumentParser()

parser.add_argument('-hid', '--hostid', type=str)
parser.add_argument('-p', '--port', type=int)
parser.add_argument('-o', '--outfile', type=str)
args = parser.parse_args()

if not args.hostid:
    HOST = '172.24.10.241'    # The default remote host, set to ras pi 
else:
    HOST = args.hostid
   
if not args.port:
    PORT = 50007              # The same port as used by the server
else:
    PORT = args.port
    
if not args.outfile:
    filename = 'pi_data_stream.csv'
else: 
    filename = args.outfile


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
print('Press Ctrl + C to exit')

# adding a header for the data 
header = ['timestamp', 'date&time', 'cjc \*C', 'cjc \micV']

for x in np.linspace(0, 15, 16, dtype='int'):
    x = str(x)
    header.append('ch' + x )
    header.append('ch' + x + ' \micV')
    header.append('ch' + x + ' \*C')


try: 

    while True:
        rec_data = s.recv(1024)
        # amount_rec += len(rec_data)
        
        if not rec_data:
            s.close()
            break
        
        rec_data = eval(rec_data)       # coverting data back to a python list
        # print (rec_data)
        
        with open(filename, 'a') as save_data_file:
            save_data_writer = csv.writer( save_data_file, delimiter=',', lineterminator='\n', quotechar="'")
            
            if file_empty_check(filename):            # adding header only if a new file
                save_data_writer.writerow(header)
                print ('New file. Header copied')
            
            save_data_writer.writerow(rec_data)        
        
    print 'Server shutdown'
    
except KeyboardInterrupt or socket.error:
    s.close()
    print'Client shutdown'
